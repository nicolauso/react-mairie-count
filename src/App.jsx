import { useState } from "react";

import "./App.css";

// un compteur
// un bouton permettant d'incrémenter le compteur
// un bouton permettant de décrémenter le compteur (il ne doit pas aller en dessous de zéro)
// un bouton permettant de remettre le compteur à zéro

function App() {
  // Déclaration d'un état et d'un seter de cet état, initialisation de count à 0
  let [count, setCount] = useState(0);

  return (
    <>
      <h1>Compteur de la mairie</h1>
      <div className="card">
        <button
          style={{ backgroundColor: "gray" }}
          // Au clic, mise à jour du state avec le seter et fonction fléchée pour décrémenter count si > 0 sinon 0
          onClick={() => setCount((count) => count > 0 ? count = count - 1 : 0)}
        >
          -
        </button>
        <h2> count is {count}</h2>
        <button
          style={{ backgroundColor: "blue" }}
          // Au clic, mise à jour de count avec le seter à count = count + 1 (incrémenter)
          onClick={() => setCount((count) => count + 1)}
        >
         +
        </button>
        
         <button style={{backgroundColor:"green"}}
         // Au clic, mise à jour de count avec le seter à count = 0 (reset) 
         onClick={() => setCount(count = 0)}>
         Reset 
        </button>  
      </div>
    </>
  );
}

export default App;
